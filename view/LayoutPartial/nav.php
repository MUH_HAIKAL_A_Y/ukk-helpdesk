<nav class="side-menu">
    <ul class="side-menu-list">

        <li class="grey with-sub">
            <a href="..\Home\">
                <i class="font-icon font-icon-dashboard"></i>
                <span class="lbl">Home</span>
            </a>
        </li>

        <li class="blue-dirty">
            <a href="..\TiketKonsultasi\">
                <i class="font-icon font-icon-comments"></i>
                <span class="lbl">Konsultasi</span>
            </a>
        </li>

        <li class="green with-sub">
            <a href="..\TiketBaru\">
                <i class=" glyphicon glyphicon-duplicate"></i>
                <span class="lbl">Tiket Baru</span>
            </a>
        </li>
    </ul>
</nav><!--.side-menu-->
<?php
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"])){
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>StartUI - Premium Bootstrap 4 Admin Dashboard Template</title>
    <?php require_once("../LayoutPartial/link.php");
    ?>
   
</head>
<body class="with-side-menu">

    <?php require_once("../LayoutPartial/header.php"); ?>

	<div class="mobile-menu-left-overlay"></div>
    <?php require_once("../LayoutPartial/nav.php");?>
	
    

	<div class="page-content">
		<div class="container-fluid">
			Blank page.
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<?php require_once("../LayoutPartial/script.php");?>
    <script src="tiketBaru.js" type="text/javascript"></script>
</body>
</html>
<?php
    }else{
        header("Location: ".connect::base_url()."index.php");
    }
?> 